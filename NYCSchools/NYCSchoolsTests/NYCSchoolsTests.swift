//
//  NYCSchoolsTests.swift
//  NYCSchoolsTests
//
//  Created by Gabriel Gomez on 12/15/19.
//  Copyright © 2019 Gabriel Gomez. All rights reserved.
//

import XCTest
@testable import NYCSchools

class NYCSchoolsTests: XCTestCase {
    
    private var mapViewModel: MapViewModel!
    
    // With more time, I would have fleshed out this test case and made sure it ran properly
    func testGetSchoolInfo() {
        
        let dbn1 = "22K405"         // This DBN should return the school: Midwood High School
        let dbn2 = "01M450"         // This DBN should return the school: East Side Community School
        let dbn3 = "32K549"         // This DBN should return the school: Bushwick School for Social Justice
        let dbn4 = "02M260"         // This DBN should return the school: Clinton School Writers & Artists, M.S. 260
        let dbn5 = "02M600"         // This DBN should return the school: The High School of Fashion Industries
        let dbn6 = "07X551"         // This DBN should return the school: The Urban Assembly Bronx Academy of Letters
        
        let midwoodHigh = "Midwood High School"
        let res = mapViewModel.schools![dbn1]?.schoolName
        XCTAssertEqual(res, midwoodHigh)
        
    }

    override func setUp() {
        // Put setup code here. This method is called before the invocation of each test method in the class.
        mapViewModel = MapViewModel()
        mapViewModel.start()
        
    }

    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
    }

    func testExample() {
        // This is an example of a functional test case.
        // Use XCTAssert and related functions to verify your tests produce the correct results.
    }

    func testPerformanceExample() {
        // This is an example of a performance test case.
        self.measure {
            // Put the code you want to measure the time of here.
        }
    }

}
