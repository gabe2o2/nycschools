//
//  Details.swift
//  NYCSchools
//
//  Created by Gabriel Gomez on 12/17/19.
//  Copyright © 2019 Gabriel Gomez. All rights reserved.
//

import UIKit

class Details: UIView {

    @IBOutlet weak var closeBtn: UIButton!
    @IBOutlet weak var backgroundView: UIView!
    @IBOutlet weak var readingScoreLbl: UILabel!
    @IBOutlet weak var mathScoreLbl: UILabel!
    @IBOutlet weak var writingScoreLbl: UILabel!
    @IBOutlet weak var apLbl: UILabel!
    @IBOutlet weak var emailLbl: UILabel!
    @IBOutlet weak var phoneLbl: UILabel!
    @IBOutlet weak var websiteLbl: UILabel!
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        commonInit()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        commonInit()
    }
    
    private func commonInit() {
        Bundle.main.loadNibNamed("Details", owner: self, options: nil)
    
        addSubview(backgroundView)
        addSubview(readingScoreLbl)
        addSubview(mathScoreLbl)
        addSubview(writingScoreLbl)
        addSubview(apLbl)
        addSubview(emailLbl)
        addSubview(phoneLbl)
        addSubview(websiteLbl)
        
        // For some reason, the "close" button is not even registering clicks
        // With more time, I would figure this out
        insertSubview(closeBtn, aboveSubview: backgroundView)
        
        backgroundView.layer.shadowColor = UIColor.black.cgColor
        backgroundView.layer.shadowOpacity = 1
        backgroundView.layer.shadowOffset = CGSize(width: 0, height: 2)
        backgroundView.layer.shadowRadius = 5
        backgroundView.layer.shouldRasterize = true   // Caches the rendered shadow as creating shadows is expensive
        
        self.autoresizingMask = [.flexibleWidth, .flexibleHeight]

    }
    
}
