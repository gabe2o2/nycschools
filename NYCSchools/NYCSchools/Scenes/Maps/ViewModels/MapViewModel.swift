//
//  MapViewModel.swift
//  NYCSchools
//
//  Created by Gabriel Gomez on 12/16/19.
//  Copyright © 2019 Gabriel Gomez. All rights reserved.
//

import Foundation

// This class handles communication from the ViewController -> ViewModel
protocol MapViewModelType {
    
    //Delegates
    var viewDelegate: MapViewModelViewDelegate? {get set}
    
    //Public properties
    var schools: [String: School]? { get }
    
    // Events
    func start()
    func getSchool(dbn: String) -> School
    func getAllSchools() -> [String: School]
}

// This class handles communication from the ViewModel -> ViewController
protocol MapViewModelViewDelegate: class {
    
    func infoRetrievalError()
}

class MapViewModel {
    // The ViewModel class is meant to store and mutate data as needed, keeping the ViewController free of data logic
    
    weak var viewDelegate: MapViewModelViewDelegate?
    
    var schools: [String: School]? = [:]
    
    func start() {
        GetSchoolInfo()
        
        // The below line, mostly the DBN and the associated school name, can be good for unit tests
//        print("this should be Midwood Highschool:\n\(schools!["22K405"])")
    }
    
    private func GetSchoolInfo() {
        // This function should 100% be made a unit test. With more time,
        // I would write out the unit tests, and provide "N/A" for places without a score or description
        // on data points I am using
        
        if let path = Bundle.main.path(forResource: "highschool_directory", ofType: "json") {
            do {
                let data = try Data(contentsOf: URL(fileURLWithPath: path), options: .mappedIfSafe)
                let jsonResult = try JSONSerialization.jsonObject(with: data, options: .mutableLeaves) as? [Any]    // Needed as the result is in an array
                
                if (jsonResult == nil) {
                    print("Trouble retrieving the JSON information for schools")
                    return
                }
                    
                for res in jsonResult! {
                    let dict = res as? [String: Any]     //Allows us to access keys by string name
                    let dbn = dict!["dbn"] as! String
                    let phone = dict!["phone_number"] as! String
                    let interest = dict!["interest1"] as! String
                    let ap = dict!["advancedplacement_courses"] as! String
                    let longitude = (dict!["Longitude"] as? NSNumber)!.floatValue
                    let latitude = (dict!["Latitude"] as? NSNumber)!.floatValue
                    let languages = dict!["language_classes"] as! String
                    let schoolName = dict!["school_name"] as! String
                    let borough = dict!["Borough"] as! String
                    let overview = dict!["overview_paragraph"] as! String
                    let address = dict!["primary_address_line_1"] as! String
                    let email = dict!["school_email"] as! String
                    let website = dict!["website"] as! String
                    
                    let school = School(dbn: dbn, schoolName: schoolName, overview: overview, languageClasses: languages, ap: ap, phone: phone, email: email, website: website, interests: interest, address: address, latitude: latitude, longitude: longitude, borough: borough)
                    
                    schools?[dbn] = school
                }

              } catch {
                // Error handler
                print("Trouble retrieving the highschool directory file. Notify user")
                
                self.viewDelegate?.infoRetrievalError()
                
                return
              }
        }
        
        if let path = Bundle.main.path(forResource: "highschool_sats", ofType: "json") {
            do {
                let data = try Data(contentsOf: URL(fileURLWithPath: path), options: .mappedIfSafe)
                let jsonResult = try JSONSerialization.jsonObject(with: data, options: .mutableLeaves) as? [Any]
                
                if (jsonResult == nil) {
                    print("Trouble retrieving the JSON information for schools")
                    return
                }
                
                for res in jsonResult! {
                    let dict = res as? [String: Any]     //Allows us to access keys by string name
                    let dbn = dict!["DBN"] as! String
                    let takers = dict!["test_takers"] as! String
                    let math = dict!["math"] as! String
                    let reading = dict!["reading"] as! String
                    let writing = dict!["writing"] as! String
                   
                    
                    let SATScores = SATs(testTakers: takers, reading: reading, math: math, writing: writing)
                    
                    schools?[dbn]?.satScores = SATScores
                }
                
            } catch {
                // Error handler
                print("Trouble retrieving the highschool SATs file. Notify user")
                
                self.viewDelegate?.infoRetrievalError()
                
                return
            }
        }
    }
}

extension MapViewModel: MapViewModelType {
    func getAllSchools() -> [String : School] {
        if (schools == nil) {
            print("Schools hasn't been loaded yet")
        }
        return schools!
    }
    
    func getSchool(dbn: String) -> School {
        if (schools == nil) {
            print("Schools hasn't been loaded yet")
        }
        return schools![dbn]!
    }
}
