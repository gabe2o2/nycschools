//
//  MapViewController.swift
//  NYCSchools
//
//  Created by Gabriel Gomez on 12/16/19.
//  Copyright © 2019 Gabriel Gomez. All rights reserved.
//

import UIKit
import MapKit

class MapViewController: UIViewController {
    
    @IBOutlet weak var mapView: MKMapView!
    
    let regionRadius: CLLocationDistance = 20000     // How far we zoom into NYC initially
    
    var detailsView: Details!
    var selectedSchool: School? = nil      // Keeps track of the currently selected school / annotation view
    
    var viewModel: MapViewModelType! {
        didSet {
            viewModel.viewDelegate = self
            viewModel.start()
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        viewModel = MapViewModel()
        mapView.delegate = self
        
        let tap = UITapGestureRecognizer(target: self, action: #selector (hideDetails)) // Temporary solution to hide the details screen
        self.view.addGestureRecognizer(tap)
        
        detailsView = Details()
        detailsView.center = CGPoint(x: 20, y: 200)
        detailsView.layer.opacity = 0
        mapView.insertSubview(detailsView, aboveSubview: mapView)
        
        let nyc = CLLocationCoordinate2D(latitude: 40.7715, longitude: -73.9206)
        centerMapOnLocation(location: nyc)
        
        populateMap()
    }
    
    func centerMapOnLocation(location: CLLocationCoordinate2D) {
        let coordinateRegion = MKCoordinateRegion(center: location,
                                                  latitudinalMeters: regionRadius, longitudinalMeters: regionRadius)
      mapView.setRegion(coordinateRegion, animated: true)
    }
    
    func populateMap() {
        let schools = Array(viewModel.getAllSchools().values)
        mapView.addAnnotations(schools)
    }
    
    @objc func hideDetails() {
        UIView.animate(withDuration: 0.25, animations: {
            self.detailsView.layer.opacity = 0
        }) { (res) in
            self.detailsView.isHidden = false
            self.mapView.isUserInteractionEnabled = true
        }
    }
}

// Code for map related functions
extension MapViewController: MKMapViewDelegate {
    
    func mapView(_ mapView: MKMapView, viewFor annotation: MKAnnotation) -> MKAnnotationView? {
        
        guard let annotation = annotation as? School else { return nil }
           
        let identifier = "marker"
        var view: MKAnnotationView
       
        if let dequeuedView = mapView.dequeueReusableAnnotationView(withIdentifier: identifier)
          as? MKMarkerAnnotationView {
          dequeuedView.annotation = annotation
          view = dequeuedView
        } else {
            view = MKAnnotationView(annotation: annotation, reuseIdentifier: identifier)
          view.canShowCallout = true
          view.calloutOffset = CGPoint(x: -5, y: 5)
          view.rightCalloutAccessoryView = UIButton(type: .detailDisclosure)
            view.image = UIImage(named: "capMarker")
       }
        
        return view
    }
    
    func mapView(_ mapView: MKMapView, didSelect view: MKAnnotationView) {
        selectedSchool = view.annotation as? School
    }
    
    func mapView(_ mapView: MKMapView, annotationView view: MKAnnotationView,
        calloutAccessoryControlTapped control: UIControl) {
        // This function is called when the callout accessory has been tapped
        // Being used to present the Details view
        
        detailsView.readingScoreLbl.text = selectedSchool?.satScores?.reading
        detailsView.mathScoreLbl.text = selectedSchool?.satScores?.math
        detailsView.writingScoreLbl.text = selectedSchool?.satScores?.writing
        detailsView.apLbl.text = selectedSchool?.APCourses
        detailsView.emailLbl.text = selectedSchool!.email
        detailsView.phoneLbl.text = selectedSchool!.phoneNumber
        detailsView.websiteLbl.text = selectedSchool!.website
        
        detailsView.isHidden = false
        mapView.isUserInteractionEnabled = false    // Useful so our user does not select a random location when closing the details screen
        UIView.animate(withDuration: 0.2) {
            self.detailsView.layer.opacity = 1
        }
    }
    
}

// Code for communication from the Map View Model
extension MapViewController: MapViewModelViewDelegate {
    func infoRetrievalError() {
        // Notify the user there was a problem with getting the highschool directory/SAT information with an alert controller
        
        let alert = UIAlertController(title: "Error", message: "We had some trouble retreiving the information for the Highschools. Please close the app and try again", preferredStyle: .alert)
        let ok = UIAlertAction(title: "Ok", style: .default, handler: nil)
        alert.addAction(ok)
        
        self.present(alert, animated: true, completion: nil)
    }
}
