//
//  Schools.swift
//  NYCSchools
//
//  Created by Gabriel Gomez on 12/16/19.
//  Copyright © 2019 Gabriel Gomez. All rights reserved.
//

import Foundation
import MapKit

struct SATs {
    var testTakers: String
    var reading: String
    var math: String
    var writing: String
}

class School: NSObject, MKAnnotation {
// A class representing a school - Marked as MKAnnotation to populate the map
    
    var dbn: String
    var schoolName: String
    var overview: String
    var languageClasses: String
    var APCourses: String
    var phoneNumber: String
    var email: String
    var website: String
    var interests: String
    var address: String
    var latitude: Float
    var longitude: Float
    var borough: String
    var satScores: SATs? = nil
    var coordinate: CLLocationCoordinate2D
    var title: String?
    var subtitle: String?
    
    init(dbn: String, schoolName: String, overview: String, languageClasses: String, ap: String, phone: String, email: String, website: String, interests: String, address: String, latitude: Float, longitude: Float, borough: String) {
        self.dbn = dbn
        self.schoolName = schoolName
        self.overview = overview
        self.languageClasses = languageClasses
        self.APCourses = ap
        self.phoneNumber = phone
        self.email = email
        self.website = website
        self.interests = interests
        self.address = address
        self.latitude = latitude
        self.longitude = longitude
        self.borough = borough
        self.coordinate = CLLocationCoordinate2D(latitude: CLLocationDegrees(latitude), longitude: CLLocationDegrees(longitude))
        self.title = self.schoolName
        self.subtitle = "Focus: \(self.interests)"
        
        super.init()
    }
}
